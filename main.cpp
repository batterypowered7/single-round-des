/*
 * main.cpp
 *
 *  Created on: Feb 27, 2015
 *      Author: Batterypowered7
 */

#include "Functions.h"

int main()
{
	//Prompt user to enter plainText and store it as a string.
	string plainText;
	cout << "Please enter plaintext to encrypt: ";
	cin >> plainText;

	//Prompt user to enter the encryption key and store it as a string.
	string key;
	cout << "Please enter encryption key: ";
	cin >> key;

	//Convert the plaintext from hexadecimal to binary and store it as a string.
	string binaryPlainText = hexToBinary(plainText);

	//Convert the key from hexadecimal to binary and store it as a string.
	string binaryKey = hexToBinary(key);

	//Split the plaintext string into left and right halves.
	string left = binaryPlainText.substr(0, 32);
	string right = binaryPlainText.substr(32, 32);

	//Expand the rightmost 32 bits to 48 bits
	char expandedRightArray[] = { right[31], right[0], right[1], right[2], right[3],
			right[4], right[3], right[4], right[5], right[6], right[7], right[8],
			right[7], right[8], right[9], right[10], right[11], right[12],
			right[11], right[12], right[13], right[14], right[15], right[16],
			right[15], right[16], right[17], right[18], right[19], right[20],
			right[19], right[20], right[21], right[22], right[23], right[24],
			right[23], right[24], right[25], right[26], right[27], right[28],
			right[27], right[28], right[29], right[30], right[31], right[0] };

	//Save the newly expanded right hand side as a string
	string expandedRightString;
	for (int i = 0; i < 48; i++)
	{
		expandedRightString += expandedRightArray[i];
	}

	//Convert left, expanded right, and key into integers. The left hand side
	//should be converted by nibble, the expanded right hand side and the key
	//should be converted in six-bit chunks.
	vector <int> integerLeft = stringToIntegerNibble(left);
	vector <int> expandedRight = stringToIntegerSixBits(expandedRightString);
	vector <int> integerKey = stringToIntegerSixBits(binaryKey);

	//Declare a vector to hold expandedRight after mangling.
	vector<int> mangled = rightMangling(expandedRight, integerKey);

	//Permute the result of mangling expandedRight and the key.
	vector <int> permutedMangled = integerToBinary(mangled);

	//Declare a vector to hold the values returned when you XOR the left hand
	//side with the permuted mangled right hand side.
	vector <int> permutedXorLeft;

	//Populate the permutedXorLeft vector with the appropriate values.
	for(int i = 0; i < 8; ++i)
	{
		permutedXorLeft.push_back(permutedMangled[i]^integerLeft[i]);
	}

	//Convert our original right hand side back into hex and save it as our new
	//left hand side.
	string newLeft = binaryToHex(right);

	//Convert our permutedXorLeft into hex and save it as our new right hand side
	string newRight = integerToHex(permutedXorLeft);

	//Output the ciphertext after a single round of DES.
	cout << "ciphertext: " << newLeft + newRight << endl;
	return 0;
}
