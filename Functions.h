/*
 * Functions.h
 *
 *  Created on: Feb 27, 2015
 *      Author: Batterypowered7
 */
#include <iostream>
#include <string>
#include <vector>
using namespace std;


#ifndef FUNCTIONS_H_
#define FUNCTIONS_H_

/******************************************************************************
*
*                         Helper Function Declarations
*
******************************************************************************/

//Function to convert our Input and Key into a binary string.
string hexToBinary(string);

//Function to convert a string of binary numbers back into hexadecimal.
string binaryToHex(string);

//Function to convert a binary number in a string variable to a binary number
//in an integer variable, six bits at a time.
vector <int> stringToIntegerSixBit(string);

//Function to convert a binary number in a string variable to a binary number
//in an integer variable, four bits at a time.
vector <int> stringToIntegerNibble(string);

//Function to convert a vector of integers into a hexadecimal string.
string integerToHex(vector<int>);

//Function to convert the mangled right hand side into a binary string, permute it
//then convert it back into a vector of integers.
vector <int> integerToBinary(vector <int>);

//Function to mangle the expanded right hand side with the key.
vector <int> rightMangling(vector <int>, vector <int>);

/******************************************************************************
*
*                         Helper Function Implementations
*
******************************************************************************/

/***************************************************************************
 * Convert a string in hexadecimal into its binary representation.
 * Precondition: A string written in hexadecimal.
 * Postcondition: A binary representation of the hexadecimal string.
 **************************************************************************/
string hexToBinary(string hexInput)
{
	string binaryValues[] =
				{ "0000", "0001", "0010", "0011", "0100", "0101", "0110", "0111",
				"1000", "1001", "1010", "1011", "1100", "1101", "1110", "1111" };
		string temp = "";
		int index;

		for (int i = 0; i < hexInput.length(); i++)
		{

			if (hexInput[i] == 'a')
			{
				index = 10;
			}

			else if (hexInput[i] == 'b')
			{
				index = 11;
			}

			else if (hexInput[i] == 'c')
			{
				index = 12;
			}

			else if (hexInput[i] == 'd')
			{
				index = 13;
			}

			else if (hexInput[i] == 'e')
			{
				index = 14;
			}

			else if (hexInput[i] == 'f')
			{
				index = 15;
			}

			else
			{
				index = hexInput[i] - '0';
			}

			temp += binaryValues[index];
		}

		return temp;
}

/***************************************************************************
 * Convert a string in binary into its hexadecimal representation.
 * Precondition: A string written in binary
 * Postcondition: A hexadecimal representation of the binary string.
 **************************************************************************/
string binaryToHex(string binaryInput)
{
	string binArray[] = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a",
			"b", "c", "d", "e", "f"};
	string temp = "";
	int index;

	for(int i = 0; i < binaryInput.length(); i+=4)
	{
		string subString = binaryInput.substr(i, 4);

		if (subString == "0000")
		{
			index = 0;
		}

		else if (subString == "0001")
		{
			index = 1;
		}

		else if (subString == "0010")
		{
			index = 2;
		}

		else if (subString == "0011")
		{
			index = 3;
		}

		else if (subString == "0100")
		{
			index = 4;
		}

		else if (subString == "0101")
		{
			index = 5;
		}

		else if (subString == "0110")
		{
			index = 6;
		}

		else if (subString == "0111")
		{
			index = 7;
		}

		else if (subString == "1000")
		{
			index = 8;
		}

		else if (subString == "1001")
		{
			index = 9;
		}

		else if (subString == "1010")
		{
			index = 10;
		}

		else if (subString == "1011")
		{
			index = 11;
		}

		else if (subString == "1100")
		{
			index = 12;
		}

		else if (subString == "1101")
		{
			index = 13;
		}

		else if (subString == "1110")
		{
			index = 14;
		}

		else if (subString == "1111")
		{
			index = 15;
		}

		temp += binArray[index];
		subString.clear();
	}

	return temp;
}

/***************************************************************************
 * Convert a binary string into a vector of integers. Each integer should be
 * six bits wide.
 * Precondition: A string written in binary.
 * Postcondition: A vector populated with integers calculated from six bit
 * chunks from the string.
 **************************************************************************/
vector <int> stringToIntegerSixBits(string binaryString)
{

	vector <int> temp;

	for(int i = 0; i < binaryString.length(); i+=6)
	{
		string subString = binaryString.substr(i, 6);

		if (subString == "000000")
		{
			temp.push_back(0);
		}

		else if (subString == "000001")
		{
			temp.push_back(1);
		}

		else if (subString == "000010")
		{
			temp.push_back(2);
		}

		else if (subString == "000011")
		{
			temp.push_back(3);
		}

		else if (subString == "000100")
		{
			temp.push_back(4);
		}

		else if (subString == "000101")
		{
			temp.push_back(5);
		}

		else if (subString == "000110")
		{
			temp.push_back(6);
		}

		else if (subString == "000111")
		{
			temp.push_back(7);
		}

		else if (subString == "001000")
		{
			temp.push_back(8);
		}

		else if (subString == "001001")
		{
			temp.push_back(9);
		}

		else if (subString == "001010")
		{
			temp.push_back(10);
		}

		else if (subString == "001011")
		{
			temp.push_back(11);
		}

		else if (subString == "001100")
		{
			temp.push_back(12);
		}

		else if (subString == "001101")
		{
			temp.push_back(13);
		}

		else if (subString == "001110")
		{
			temp.push_back(14);
		}

		else if (subString == "001111")
		{
			temp.push_back(15);
		}

		else if (subString == "010000")
		{
			temp.push_back(16);
		}

		else if (subString == "010001")
		{
			temp.push_back(17);
		}

		else if (subString == "010010")
		{
			temp.push_back(18);
		}

		else if (subString == "010011")
		{
			temp.push_back(19);
		}

		else if (subString == "010100")
		{
			temp.push_back(20);
		}

		else if (subString == "010101")
		{
			temp.push_back(21);
		}

		else if (subString == "010110")
		{
			temp.push_back(22);
		}

		else if (subString == "010111")
		{
			temp.push_back(23);
		}

		else if (subString == "011000")
		{
			temp.push_back(24);
		}

		else if (subString == "011001")
		{
			temp.push_back(25);
		}

		else if (subString == "011010")
		{
			temp.push_back(26);
		}

		else if (subString == "011011")
		{
			temp.push_back(27);
		}

		else if (subString == "011100")
		{
			temp.push_back(28);
		}

		else if (subString == "011101")
		{
			temp.push_back(29);
		}

		else if (subString == "011110")
		{
			temp.push_back(30);
		}

		else if (subString == "011111")
		{
			temp.push_back(31);
		}

		else if (subString == "100000")
		{
			temp.push_back(32);
		}

		else if (subString == "100001")
		{
			temp.push_back(33);
		}

		else if (subString == "100010")
		{
			temp.push_back(34);
		}

		else if (subString == "100011")
		{
			temp.push_back(35);
		}

		else if (subString == "100100")
		{
			temp.push_back(36);
		}

		else if (subString == "100101")
		{
			temp.push_back(37);
		}

		else if (subString == "100110")
		{
			temp.push_back(38);
		}

		else if (subString == "100111")
		{
			temp.push_back(39);
		}

		else if (subString == "101000")
		{
			temp.push_back(40);
		}

		else if (subString == "101001")
		{
			temp.push_back(41);
		}

		else if (subString == "101010")
		{
			temp.push_back(42);
		}

		else if (subString == "101011")
		{
			temp.push_back(43);
		}

		else if (subString == "101100")
		{
			temp.push_back(44);
		}

		else if (subString == "101101")
		{
			temp.push_back(45);
		}

		else if (subString == "101110")
		{
			temp.push_back(46);
		}

		else if (subString == "101111")
		{
			temp.push_back(47);
		}

		else if (subString == "110000")
		{
			temp.push_back(48);
		}

		else if (subString == "110001")
		{
			temp.push_back(49);
		}

		else if (subString == "110010")
		{
			temp.push_back(50);
		}

		else if (subString == "110011")
		{
			temp.push_back(51);
		}

		else if (subString == "110100")
		{
			temp.push_back(52);
		}

		else if (subString == "110101")
		{
			temp.push_back(53);
		}

		else if (subString == "110110")
		{
			temp.push_back(54);
		}

		else if (subString == "110111")
		{
			temp.push_back(55);
		}

		else if (subString == "111000")
		{
			temp.push_back(56);
		}

		else if (subString == "111001")
		{
			temp.push_back(57);
		}

		else if (subString == "111010")
		{
			temp.push_back(58);
		}

		else if (subString == "111011")
		{
			temp.push_back(59);
		}

		else if (subString == "111100")
		{
			temp.push_back(60);
		}

		else if (subString == "111101")
		{
			temp.push_back(61);
		}

		else if (subString == "111110")
		{
			temp.push_back(62);
		}

		else if (subString == "111111")
		{
			temp.push_back(63);
		}

		subString.clear();
	}

	return temp;
}

/***************************************************************************
 * Convert a binary string into a vector of integers. Each integer should be
 * four bits wide.
 * Precondition: A string written in binary.
 * Postcondition: A vector populated with integers calculated from four bit
 * chunks (nibbles) from the string.
 **************************************************************************/
vector <int> stringToIntegerNibble(string binaryString)
{
	vector <int> temp;

	for(int i = 0; i < binaryString.length(); i+=4)
	{
		string subString = binaryString.substr(i, 4);

		if (subString == "0000")
		{
			temp.push_back(0);
		}

		else if (subString == "0001")
		{
			temp.push_back(1);
		}

		else if (subString == "0010")
		{
			temp.push_back(2);
		}

		else if (subString == "0011")
		{
			temp.push_back(3);
		}

		else if (subString == "0100")
		{
			temp.push_back(4);
		}

		else if (subString == "0101")
		{
			temp.push_back(5);
		}

		else if (subString == "0110")
		{
			temp.push_back(6);
		}

		else if (subString == "0111")
		{
			temp.push_back(7);
		}

		else if (subString == "1000")
		{
			temp.push_back(8);
		}

		else if (subString == "1001")
		{
			temp.push_back(9);
		}

		else if (subString == "1010")
		{
			temp.push_back(10);
		}

		else if (subString == "1011")
		{
			temp.push_back(11);
		}

		else if (subString == "1100")
		{
			temp.push_back(12);
		}

		else if (subString == "1101")
		{
			temp.push_back(13);
		}

		else if (subString == "1110")
		{
			temp.push_back(14);
		}

		else if (subString == "1111")
		{
			temp.push_back(15);
		}

		subString.clear();
	}

	return temp;
}

/***************************************************************************
 * Convert a vector of integers into their hexadecimal representation.
 * Precondition: A vector of integers.
 * Postcondition: A hexadecimal representation of the vector returned as
 * a string.
 **************************************************************************/
string integerToHex(vector <int> integerVector)
{
	string temp = "";
	string hexArray[] = {"0", "1", "2", "3", "4", "5", "6", "7",
								"8", "9", "a", "b", "c", "d", "e", "f",};

	for(int i = 0; i < integerVector.size(); ++i)
	{
		temp += hexArray[integerVector[i]];
	}

	return temp;
}

/***************************************************************************
 * Function to convert the mangled right hand side into a binary string,
 * permute it then convert it back into a vector of integers.
 * Precondition: A vector of integers.
 * Postcondition: A vector that stores a permutation of the mangled right
 * hand side
 **************************************************************************/
vector <int> integerToBinary(vector <int> integerVector)
{
	string temp = "";
	string binaryArray[] = {"0000", "0001", "0010", "0011", "0100", "0101", "0110", "0111",
									"1000", "1001", "1010", "1011", "1100", "1101", "1110", "1111",};

	for(int i = 0; i < integerVector.size(); ++i)
	{
		temp += binaryArray[integerVector[i]];
	}

	string tempTwo;

	tempTwo.push_back(temp[15]);
	tempTwo.push_back(temp[6]);
	tempTwo.push_back(temp[19]);
	tempTwo.push_back(temp[20]);
	tempTwo.push_back(temp[28]);
	tempTwo.push_back(temp[11]);
	tempTwo.push_back(temp[27]);
	tempTwo.push_back(temp[16]);
	tempTwo.push_back(temp[0]);
	tempTwo.push_back(temp[14]);
	tempTwo.push_back(temp[22]);
	tempTwo.push_back(temp[25]);
	tempTwo.push_back(temp[4]);
	tempTwo.push_back(temp[17]);
	tempTwo.push_back(temp[30]);
	tempTwo.push_back(temp[9]);
	tempTwo.push_back(temp[1]);
	tempTwo.push_back(temp[7]);
	tempTwo.push_back(temp[23]);
	tempTwo.push_back(temp[13]);
	tempTwo.push_back(temp[31]);
	tempTwo.push_back(temp[26]);
	tempTwo.push_back(temp[2]);
	tempTwo.push_back(temp[8]);
	tempTwo.push_back(temp[18]);
	tempTwo.push_back(temp[12]);
	tempTwo.push_back(temp[29]);
	tempTwo.push_back(temp[5]);
	tempTwo.push_back(temp[21]);
	tempTwo.push_back(temp[10]);
	tempTwo.push_back(temp[3]);
	tempTwo.push_back(temp[24]);


	vector <int> tempThree = stringToIntegerNibble(tempTwo);

	return tempThree;
}

/***************************************************************************
 * Function to mangle the expanded right hand side with the key.
 * Precondition: The expanded right hand side and the key, stored as
 * integer vectors.
 * Postcondition: A vector storing the mangled values for the right hand
 * side.
 **************************************************************************/
vector <int> rightMangling(vector <int> expandedRight, vector <int> integerKey)
{

	int sBox [8][4][16]= {{ { 14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7 },
			 { 0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8 },
			 { 4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0 },
			 { 15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13 }},
			 {{ 15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10 },
			 { 3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5 },
			 { 0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15 },
			 { 13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9 }},
			 {{ 10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8 },
			 { 13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1 },
			 { 13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7 },
			 { 1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12 }},
			 {{ 7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15 },
			 { 13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9 },
			 { 10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4 },
			 { 3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14 }},
			 {{ 2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9 },
			 { 14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6 },
			 { 4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14 },
			 { 11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3 }},
			 {{ 12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11 },
			 { 10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8 },
			 { 9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6 },
			 { 4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13 }},
			 {{ 4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1 },
			 { 13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6 },
			 { 1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2 },
			 { 6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12 }},
			 {{ 13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7 },
			 { 1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2 },
			 { 7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8 },
			 { 2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11 }}};

	vector<int> mangled;
	for(int i = 0; i < 8; ++ i)
	{
		int rightXorKey = expandedRight[i]^integerKey[i];

		switch(rightXorKey)
		{
			case 0:
				mangled.push_back(sBox[i][0][0]);
				break;
			case 1:
				mangled.push_back(sBox[i][1][0]);
				break;
			case 2:
				mangled.push_back(sBox[i][0][1]);
				break;
			case 3:
				mangled.push_back(sBox[i][1][1]);
				break;
			case 4:
				mangled.push_back(sBox[i][0][2]);
				break;
			case 5:
					mangled.push_back(sBox[i][1][2]);
				break;
			case 6:
					mangled.push_back(sBox[i][0][3]);
				break;
			case 7:
					mangled.push_back(sBox[i][1][3]);
				break;
			case 8:
					mangled.push_back(sBox[i][0][4]);
				break;
			case 9:
					mangled.push_back(sBox[i][1][4]);
				break;
			case 10:
					mangled.push_back(sBox[i][0][5]);
				break;
			case 11:
					mangled.push_back(sBox[i][1][5]);
				break;
			case 12:
					mangled.push_back(sBox[i][0][6]);
				break;
			case 13:
					mangled.push_back(sBox[i][1][6]);
				break;
			case 14:
					mangled.push_back(sBox[i][0][7]);
				break;
			case 15:
					mangled.push_back(sBox[i][1][7]);
				break;
			case 16:
					mangled.push_back(sBox[i][0][8]);
				break;
			case 17:
					mangled.push_back(sBox[i][1][8]);
				break;
			case 18:
					mangled.push_back(sBox[i][0][9]);
				break;
			case 19:
					mangled.push_back(sBox[i][1][9]);
				break;
			case 20:
					mangled.push_back(sBox[i][0][10]);
				break;
			case 21:
					mangled.push_back(sBox[i][1][10]);
				break;
			case 22:
					mangled.push_back(sBox[i][0][11]);
				break;
			case 23:
					mangled.push_back(sBox[i][1][11]);
				break;
			case 24:
					mangled.push_back(sBox[i][0][12]);
				break;
			case 25:
					mangled.push_back(sBox[i][1][12]);
				break;
			case 26:
					mangled.push_back(sBox[i][0][13]);
				break;
			case 27:
					mangled.push_back(sBox[i][1][13]);
				break;
			case 28:
					mangled.push_back(sBox[i][0][14]);
				break;
			case 29:
					mangled.push_back(sBox[i][1][14]);
				break;
			case 30:
					mangled.push_back(sBox[i][0][15]);
				break;
			case 31:
					mangled.push_back(sBox[i][1][15]);
				break;
			case 32:
					mangled.push_back(sBox[i][2][0]);
				break;
			case 33:
					mangled.push_back(sBox[i][3][0]);
				break;
			case 34:
					mangled.push_back(sBox[i][2][1]);
				break;
			case 35:
					mangled.push_back(sBox[i][3][1]);
				break;
			case 36:
					mangled.push_back(sBox[i][2][2]);
				break;
			case 37:
					mangled.push_back(sBox[i][3][2]);
				break;
			case 38:
					mangled.push_back(sBox[i][2][3]);
				break;
			case 39:
					mangled.push_back(sBox[i][3][3]);
				break;
			case 40:
					mangled.push_back(sBox[i][2][4]);
				break;
			case 41:
					mangled.push_back(sBox[i][3][4]);
				break;
			case 42:
					mangled.push_back(sBox[i][2][5]);
				break;
			case 43:
					mangled.push_back(sBox[i][3][5]);
				break;
			case 44:
					mangled.push_back(sBox[i][2][6]);
				break;
			case 45:
					mangled.push_back(sBox[i][3][6]);
				break;
			case 46:
					mangled.push_back(sBox[i][2][7]);
				break;
			case 47:
					mangled.push_back(sBox[i][3][7]);
				break;
			case 48:
					mangled.push_back(sBox[i][2][8]);
				break;
			case 49:
					mangled.push_back(sBox[i][3][8]);
				break;
			case 50:
					mangled.push_back(sBox[i][2][9]);
				break;
			case 51:
					mangled.push_back(sBox[i][3][9]);
				break;
			case 52:
					mangled.push_back(sBox[i][2][10]);
				break;
			case 53:
					mangled.push_back(sBox[i][3][10]);
				break;
			case 54:
					mangled.push_back(sBox[i][2][11]);
				break;
			case 55:
					mangled.push_back(sBox[i][3][11]);
				break;
			case 56:
					mangled.push_back(sBox[i][2][12]);
				break;
			case 57:
					mangled.push_back(sBox[i][3][12]);
				break;
			case 58:
					mangled.push_back(sBox[i][2][13]);
				break;
			case 59:
					mangled.push_back(sBox[i][3][13]);
				break;
			case 60:
					mangled.push_back(sBox[i][2][14]);
				break;
			case 61:
					mangled.push_back(sBox[i][3][14]);
				break;
			case 62:
					mangled.push_back(sBox[i][2][15]);
				break;
			case 63:
					mangled.push_back(sBox[i][3][15]);
				break;
			default:
				break;
		}
	}

	return mangled;
}

#endif /* FUNCTIONS_H_ */
